const express = require('express')
const db = require('./db')
const utils = require('./utils')

const app = express()

app.use(express.json())




app.get('/', (request, response) => {
  const query = `
    SELECT * FROM Movie
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})


app.post('/', (request, response) => {

    const {Title, Date, Time, Director} = request.body

    const query = `
     INSERT INTO Movie ( movie_title, movie_release_date,movie_time,director_name)
     VALUES ( '${Title}', '${Date}','${Time}', '${Director}')
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

  app.put('/:id', (request, response) => {

    const { Date, Time} = request.body
    const {id}= request.params
   
    const query = `
     UPDATE Movie SET  movie_release_date= '${Date}', movie_time = '${Time}'
     Where movie_id= ${id})
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

  
  app.delete('/:id', (request, response) => {

    const {id}= request.params

    const query = `
     DELETE FROM Movie Where movie_id= '${id}')
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

 

app.listen(4000, '0.0.0.0', () => {
  console.log('category-server started on port 4000')
})

