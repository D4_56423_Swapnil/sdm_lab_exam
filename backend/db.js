const mysql = require('mysql2')

const pool = mysql.createPool({
  host: 'labdata',
  user: 'root',
  password: 'root',
  database: 'LabData',
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
})

module.exports = pool