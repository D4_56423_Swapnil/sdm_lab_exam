CREATE TABLE Movie (
    movie_id INTEGER PRIMARY KEY auto_increment,
    movie_title VARCHAR(100),
    movie_release_date VARCHAR(100),
    movie_time VARCHAR(50),
    director_name VARCHAR(100)
);

INSERT INTO Movie ( movie_title, movie_release_date, movie_time, director_name) 
    VALUES ('Spider-man', '1 Jan 2022', '12 pm', 'Jon Watts');